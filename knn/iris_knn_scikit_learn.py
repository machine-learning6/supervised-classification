from sklearn.neighbors import KNeighborsClassifier
import sklearn.datasets as ds

iris = ds.load_iris()

features = iris["data"].tolist()
classes = iris["target"]
print(features)
print(classes)

model = KNeighborsClassifier(n_neighbors=1)
model.fit(features, classes)

predicted = model.predict([[6.3, 3.3, 4.7, 1.6]])
print("The class is : ", predicted)
