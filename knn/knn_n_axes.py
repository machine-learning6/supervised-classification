import matplotlib.pyplot as plt  # graphics
import random


# DATA HAS TO BE LIKE : [[][][]]
def clean_data_knn(iris_class, iris_data):  # put the data on the good format
    data = []
    for i in range(len(iris_class)):
        data.append([])
        for j in range(len(iris_data) // len(iris_class)):
            data[i].append((iris_data[i * len(iris_data[0]) + j]).tolist())
    return data


def generate_point(nb_axes=2, tendency=1):
    point = []
    for i in range(nb_axes):
        point.append(random.random() * (tendency + 1))
    return point


def generate_data(nb_classes=2, nb_points=4, nb_axes=2):
    data = []
    for i in range(nb_classes):
        data.append([])
        for j in range(nb_points):
            point = generate_point(nb_axes, i)
            data[i].append(point)
    return data


def display_data(data):
    fig = plt.figure()
    axes = fig.add_axes([0, 0, 1, 1])
    for i in range(len(data)):
        axes.scatter(*zip(*data[i]))
    plt.show()


def calculation_distance(point_initial, point_neighbour):
    nb_axes = len(point_neighbour)
    distance = 0
    for i in range(nb_axes):
        distance = distance + (point_initial[i] - point_neighbour[i]) ** 2
    distance = distance ** (1 / 2)

    return distance


def calculation_distances(data, point_initial):
    distances = []

    for i in range(len(data)):
        distances.append([])
        for point_tested in data[i]:
            distances[i].append(calculation_distance(point_initial, point_tested))

    return distances  # return a list of distances in the same order than data


def leading_class(list_distances, k):
    nb_classes = len(list_distances)
    nb_points = len(list_distances[0])

    class_k_neighbours = [0 for _ in range(nb_classes)]
    storage_indices = []

    for i in range(k):  # man tests the K nearest neighbours
        indice_min = [0, 0]
        min = 1000
        for j in range(nb_classes):
            for k in range(nb_points):
                distance_to_compare = list_distances[j][k]
                if (distance_to_compare < min) and not ([j, k] in storage_indices):
                    indice_min = [j, k]
                    min = distance_to_compare
        class_k_neighbours[indice_min[0]] += 1
        storage_indices.append(indice_min)

    return class_k_neighbours.index(max(class_k_neighbours))


def main(data, point, k):
    list_distances = calculation_distances(data, point)
    classe = leading_class(list_distances, k)

    return classe


if __name__ == "__main__":
    random.seed(2)
    nb_classes = 2
    axes = 2
    nb_points = 30
    k = 2
    data = generate_data(nb_classes, nb_points, axes)
    point = generate_point(axes)
    classe = main(data, point, k)
    print("Your point belongs to : class n°", classe)

    data.append([point])
    display_data(data)
