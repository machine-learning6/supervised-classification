import sklearn.datasets as ds
import knn_n_axes as knn


def clean_iris() :
    iris = ds.load_iris()  # load.iris() est une fonction du package sklearn.dataset
    iris_data = iris["data"]
    iris_target = iris["target"]
    iris_class = iris["target_names"]
    iris_descr = iris["DESCR"]

    data = knn.clean_data_knn(iris_class, iris_data)
    return data

def main(point, k, data) :
    classe = knn.main(data,point,k)
    return classe

if __name__ == "__main__":
    point = [6.3, 3.3, 4.7, 1.6]
    k = 10
    data = clean_iris()
    classe = main(point,k, data)
    print("Your class is : ", classe)
