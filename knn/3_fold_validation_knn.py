# mélanger les data
# diviser en 3 groupes de taille égales (150/3)
# appliquer l'algorithme knn sur 2 groupes puis vérifier le résultat avec le 3ème
# répéter le process 3 fois pour valider l'algo

# étape 1 : importation de IRIS
import sklearn.datasets as ds
import random

# étape 3 : diviser les données en 3 groupes
def split_list_k(data_set, nb_splits = 3) :
    data = []
    for i in range(nb_splits) :
        data.append([])
        for j in range(len(data_set)//nb_splits) :
            data[i].append(data_set[(i*len(data_set)//nb_splits) + j])
    print(data)
    return data

def slice_list_k(data_set, nb_splits = 3) :
    n = len(data_set)
    k = n // nb_splits
    data = [data_set[i*k:(i+1)*k] for i in range(nb_splits)] # méthode de slice : [début, fin]
    return data

if __name__ == "__main__":

    iris_db = ds.load_iris()
    iris_features = iris_db["data"].tolist()
    iris_classes = iris_db["target"]
    #print(iris_features)
    #print(iris_classes)

    # étape 2 : mélanger les données
    lien = list(zip(iris_classes, iris_features))
    random.shuffle(lien)
    iris_classes, iris_features = zip(*lien)
    iris_classes = list(iris_classes)
    iris_features = list(iris_features)

    #print(iris_features)
    #print(iris_classes)

    test = [6, 9,4,90, 8, 5]
    test_split = slice_list_k(test,3)
    print(test_split)

    #split_features = split_list_k(iris_features,3)
    #split_classes = split_list_k(iris_classes,3)




