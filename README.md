# Supervised classification

## Purpose of the project 
Reference the different kinds of supervised classification (such as KNN) and create a personal portefolio 

## How to install it ? 
 
> git clone https://gitlab.com/machine-learning6/supervised-classification

> pip install sklearn

> pip install matplotlib 

## Useful links
Exercices : http://www.oliviergibaru.org/courses/ML_Supervised.html